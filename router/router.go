package router

import (
	"net/http"

	"github.com/gorilla/mux"

	"packs/pkg/packs"
)

func New(handler packs.Handler) *mux.Router {
	router := mux.NewRouter()

	router.Path("/").
		Methods(http.MethodGet, http.MethodPost).
		Handler(http.FileServer(http.Dir("./static"))).
		Name("static")

	router.Path("/health").
		Methods(http.MethodGet).
		HandlerFunc(Health).
		Name("health")

	router.Path("/pack-sizes").
		Methods(http.MethodPost).
		HandlerFunc(handler.PacksSizes).
		Name("root")

	router.Path("/packs").
		Methods(http.MethodPost).
		HandlerFunc(handler.PacksAmount).
		Name("root")

	return router
}

// Health returns 200 OK for every request
func Health(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}
