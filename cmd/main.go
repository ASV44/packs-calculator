package main

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"time"

	"packs/pkg/packs"
	"packs/router"
)

const defaultTimeout = 10 * time.Second

func main() {
	rawPackSizes := strings.Split(os.Getenv("PACK_SIZES"), ",")
	packSizes := make([]int, 0, len(rawPackSizes))
	for _, packSizeRaw := range rawPackSizes {
		packSize, err := strconv.Atoi(packSizeRaw)
		if err != nil {
			slog.Error("failed to parse pack size value", "err", err)
			return
		}

		packSizes = append(packSizes, packSize)
	}

	packsReqHandler := packs.NewHandler(packs.NewService(packSizes))

	server := http.Server{
		Addr:         fmt.Sprintf(":%s", os.Getenv("PORT")),
		Handler:      router.New(packsReqHandler),
		ReadTimeout:  defaultTimeout,
		WriteTimeout: defaultTimeout,
	}

	// Start server
	go func() {
		if err := server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			slog.Error("shutting down the server", "err", err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds.
	// Use a buffered channel to avoid missing signals as recommended for signal.Notify
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), defaultTimeout)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		slog.Error("failed to shutdown the server", "err", err)
	}
}
