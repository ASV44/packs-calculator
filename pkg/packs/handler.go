package packs

import (
	"encoding/json"
	"net/http"
)

type Handler struct {
	packsService Packs
}

func NewHandler(packsService Packs) Handler {
	return Handler{packsService: packsService}
}

func (h *Handler) PacksSizes(w http.ResponseWriter, req *http.Request) {
	var packSizes Sizes
	if err := json.NewDecoder(req.Body).Decode(&packSizes); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	h.packsService = NewService(packSizes.PackSizes)

	w.WriteHeader(http.StatusNoContent)
}

func (h *Handler) PacksAmount(w http.ResponseWriter, req *http.Request) {
	var order Order
	if err := json.NewDecoder(req.Body).Decode(&order); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	shipment := h.packsService.GetPacks(order.Items)

	resp, err := json.Marshal(Shipment{Packs: shipment})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(resp)
}
