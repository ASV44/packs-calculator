package packs

import (
	"math"
	"slices"
)

// Packs defines operation for calculating packs
type Packs interface {
	GetPacks(items int) map[int]int
}

// Service calculates minimal number of packs required to send items from order based on dynamic programming
type Service struct {
	packSizes []int
	// packsPerItems stores the minimum amount of packs required for items in order
	// Index of packsPerItems represents the number of items in order
	// Value for specific index represents minimal number of packs for this items number
	packsPerItems []int
	// itemsPerOrder stores the minimum amount of items to be shipped
	// Index of itemsPerOrder represents the number of items in order
	// Value for specific index represents minimal number of items which will be shipped by all packs
	itemsPerOrder []int
}

func NewService(packSizes []int) *Service {
	slices.Sort(packSizes)

	return &Service{
		packSizes: packSizes,
		// Init packsPerItems as for 0 items we provide 0 packs
		packsPerItems: []int{0},
		// Init itemsPerOrder as for 0 items we provide 0 total items
		itemsPerOrder: []int{0},
	}
}

// GetPacks calculates which packs would be used in order to send order items
// based on calculated numbers of minimal packs for order items
func (s *Service) GetPacks(items int) map[int]int {
	packsToBeSent := make(map[int]int)
	if items > len(s.packsPerItems)-1 {
		s.calculatePacketsPerItems(items)
	}

	for items > 0 {
		for _, packSize := range s.packSizes {
			remainingItems := items - packSize
			if remainingItems < 0 {
				remainingItems = 0
			}

			// If number of packs for remaining items is equal to number of packs for current minus 1 pack
			// this mean we can use current pack size as this is value was use during recurrent function calculation
			if s.packsPerItems[remainingItems] == s.packsPerItems[items]-1 && s.itemsPerOrder[remainingItems] == s.itemsPerOrder[items]-packSize {
				packsToBeSent[packSize] += 1
				items = remainingItems
				break
			}
		}
	}

	return packsToBeSent
}

// calculatePacketsPerItems uses dynamic programming method for calculating minimal number of packs to be used for items
// It iterates until required number of items and stores in packsPerItems minimal number of packets for each item amount before required.
func (s *Service) calculatePacketsPerItems(items int) {
	for item := len(s.packsPerItems); item <= items; item++ {
		// add max possible int value in order to compare with smaller possible values as we interested in the minimal packs and items amount
		s.packsPerItems = append(s.packsPerItems, math.MaxInt)
		s.itemsPerOrder = append(s.itemsPerOrder, math.MaxInt)
		for _, packSize := range s.packSizes {
			// If number of items is smaller or equal when pack size
			// and total number of items in pack is smaller or equal when total of selected packs
			// we can send these items using this pack
			if item <= packSize && packSize <= s.itemsPerOrder[item] {
				s.packsPerItems[item] = 1
				s.itemsPerOrder[item] = packSize
			}

			// For larger items set we identify previous minimal number of items
			if item > packSize &&
				s.itemsPerOrder[item-packSize]+packSize <= s.itemsPerOrder[item] {
				// Add one more pack to number of packs and pack size to number of items
				s.packsPerItems[item] = s.packsPerItems[item-packSize] + 1
				s.itemsPerOrder[item] = s.itemsPerOrder[item-packSize] + packSize
			}
		}
	}
}
