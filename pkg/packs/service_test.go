package packs

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type serviceTestSuite struct {
	suite.Suite
	packSizes []int
	service   *Service
}

func (s *serviceTestSuite) SetupTest() {
	s.packSizes = []int{250, 500, 1000, 2000, 5000}
	s.service = NewService(s.packSizes)
}

func TestServiceSuite(t *testing.T) {
	suite.Run(t, new(serviceTestSuite))
}

func (s *serviceTestSuite) Test_GetPacks_CorrectPacksCalculated() {
	testCases := []struct {
		name     string
		items    int
		expected map[int]int
	}{
		{
			name:     "less_than_smallest_pack",
			items:    s.packSizes[0] - 1,
			expected: map[int]int{s.packSizes[0]: 1},
		},
		{
			name:     "equal_to_smallest_pack",
			items:    s.packSizes[0],
			expected: map[int]int{s.packSizes[0]: 1},
		},
		{
			name:     "one_big_pack_instead_of_two_small",
			items:    s.packSizes[0] + 1,
			expected: map[int]int{s.packSizes[1]: 1},
		},
		{
			name:     "more_packs_but_less_items",
			items:    s.packSizes[1] + 1,
			expected: map[int]int{s.packSizes[0]: 1, s.packSizes[1]: 1},
		},
		{
			name:     "uniform_packs_distribution",
			items:    12001,
			expected: map[int]int{s.packSizes[0]: 1, s.packSizes[3]: 1, s.packSizes[4]: 2},
		},
	}

	for _, tc := range testCases {
		s.T().Run(tc.name, func(t *testing.T) {
			shipment := s.service.GetPacks(tc.items)
			s.Equal(tc.expected, shipment)
		})
	}
}
