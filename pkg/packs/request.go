package packs

type Sizes struct {
	PackSizes []int `json:"packSizes,omitempty"`
}

type Order struct {
	Items int `json:"items,omitempty"`
}
