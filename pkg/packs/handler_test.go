package packs

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/suite"

	"packs/pkg/packs/mocks"
)

type handlerTestSuite struct {
	suite.Suite
	mockPacks *mocks.Packs
	handler   Handler
}

func (s *handlerTestSuite) SetupTest() {
	s.mockPacks = new(mocks.Packs)
	s.handler = NewHandler(s.mockPacks)
}

func TestHandlerSuite(t *testing.T) {
	suite.Run(t, new(handlerTestSuite))
}

func (s *handlerTestSuite) Test_PacksAmount_Success() {
	recorder := httptest.NewRecorder()
	order := Order{Items: 499}
	reqData, err := json.Marshal(order)
	s.NoError(err)
	req := httptest.NewRequest(http.MethodPost, "/packs", bytes.NewReader(reqData))
	expectedShipment := map[int]int{500: 1}
	s.mockPacks.On("GetPacks", order.Items).Return(expectedShipment, nil)
	expectedResp := Shipment{Packs: expectedShipment}

	s.handler.PacksAmount(recorder, req)

	result := recorder.Result()
	var resp Shipment
	s.NoError(json.NewDecoder(result.Body).Decode(&resp))
	s.Equal(expectedResp, resp)
}
