# Order Packs Calculator

Sample project which calculates required packs for order items based on product pack sizes.

## Build and Deploy

In order to run project on local environment use`docker-compose`.

1. From project root run `docker-compose up`
2. Perform `POST` request to `localhos:8000/packs`

Request Payload
```JSON
{
  "items": 501
}
```

## Configuration:
In order to change pack sizes just change configuration in `docker-compose` of `PACK_SIZES` environment variable.

## Calculation

Number of packs is calculated using dynamic programming.
This allows calculation of packs numbers more efficient comparing greedy algorithm.
Dynamic calculation allow reuse of previous calculated results.
The packs numbers are calculated for new items only if current calculation is less than previous calculated order items size.
Calculation algorithm minimize number of packs and number of items. 
