############################
# STEP 1 build executable binary
############################

FROM golang:alpine AS builder

# Set environment variables

ENV SERVICE api
ENV WORKDIR /${SERVICE}

# Change container work directory

WORKDIR $WORKDIR

# Copy source code to build image

COPY . .

# Fetch dependencies & Build the binary.

RUN go build -o /go/bin/main cmd/main.go

###############################################
# STEP 2 build a small image, production build
##############################################

FROM alpine

# Set environment variables

ENV WORKDIR /go/bin

# Copy our static executable from builder

COPY --from=builder $WORKDIR/main $WORKDIR/
COPY --from=builder /api/static/ $WORKDIR/static/

# Change container work directory

WORKDIR $WORKDIR

# Run the binary.
ENTRYPOINT ["/go/bin/main"]
